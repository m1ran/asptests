﻿using System.Collections.Generic;
using MySQL.Entities;

namespace MySQL.Abstract
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> Categories { get; }
    }
}