﻿using System.Collections.Generic;
using MySQL.Entities;

namespace MySQL.Abstract
{
    public interface IProductRepository
    {
        IEnumerable<Product> Products { get; }

        void SaveProduct(Product product);

        Product DeleteProduct(int productID);
    }
}