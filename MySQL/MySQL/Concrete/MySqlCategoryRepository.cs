﻿using MySQL.Abstract;
using MySQL.Entities;
using System.Collections.Generic;

namespace MySQL.Concrete
{
    public class MySqlCategoryRepository : ICategoryRepository
    {
        private MySqlDbContext context = new MySqlDbContext();

        public IEnumerable<Category> Categories
        {
            get { return context.Categories; }
        }
    }
}