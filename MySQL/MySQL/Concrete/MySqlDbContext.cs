﻿using System.Data.Entity;
using MySQL.Entities;

namespace MySQL.Concrete
{
    [DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class MySqlDbContext : DbContext
    {
        public MySqlDbContext(): base("MySqlContext")
        { }

        public DbSet<Product> Products { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}