﻿using MySQL.Abstract;
using MySQL.Entities;
using System.Collections.Generic;

namespace MySQL.Concrete
{
    public class MySqlProductRepository : IProductRepository
    {
        private MySqlDbContext context = new MySqlDbContext();

        public IEnumerable<Product> Products
        {
            get { return context.Products; }
        }

        public void SaveProduct(Product product)
        {
            if (product.id == 0)
            {
                context.Products.Add(product);
            }
            else
            {
                Product dbEntry = context.Products.Find(product.id);
                if (dbEntry != null)
                {
                    dbEntry.name = product.name;
                    dbEntry.description = product.description;
                    dbEntry.price = product.price;
                    dbEntry.category_id = product.category_id;
                }
            }
            context.SaveChanges();
        }

        public Product DeleteProduct(int productID)
        {
            Product dbEntry = context.Products.Find(productID);
            if (dbEntry != null)
            {
                context.Products.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}