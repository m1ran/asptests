﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MySQL.Abstract;
using MySQL.Entities;
using MySQL.Concrete;

namespace MySQL.Controllers
{
    public class HomeController : Controller
    {
        private IProductRepository repository;

        public HomeController(IProductRepository productRepository)
        {
            this.repository = productRepository;
        }
        // GET: Home
        public ActionResult Index()
        {
            return View(repository.Products);
        }

        public ActionResult Create()
        {
            return View("Edit", new Product());
        }

        public ActionResult Edit(int? id)
        {
            Product product = repository.Products.FirstOrDefault(p => p.id == id);
            if (product != null)
            {
                return View(product);
            }
            else
            {
                return RedirectToAction("Index");
            }
            
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Product deletedProduct = repository.DeleteProduct(id);
            if (deletedProduct != null)
            {
                TempData["message"] = string.Format("{0} was deleted",
                deletedProduct.name);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(Product product)
        {
            if (ModelState.IsValid)
            {
                repository.SaveProduct(product);
                TempData["message"] = string.Format("{0} has been saved", product.name);
                return RedirectToAction("Index");
            }
            else
            {
                return View(product);
            }
        }
    }
}