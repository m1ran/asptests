﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MySQL.Entities.Metadata
{
    public partial class ProductMetaData
    {
        [HiddenInput(DisplayValue = false)]
        public int id { get; set; }

        [Display(Name = "Name")]
        [MinLength(2)]
        [Required(ErrorMessage = "Please enter a product name")]
        public string name { get; set; }

        [Display(Name = "Description")]
        [MinLength(2)]
        [Required(ErrorMessage = "Please enter a description")]
        public string description { get; set; }

        [Display(Name = "Price")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Please enter a positive price")]
        public decimal price { get; set; }

        [Display(Name = "Category")]
        [Required(ErrorMessage = "Please select category")]
        public int category_id { get; set; }
    }
}