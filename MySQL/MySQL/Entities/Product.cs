﻿using MySQL.Entities.Metadata;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MySQL.Entities
{
    [MetadataType(typeof(ProductMetaData))]
    public partial class Product
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public decimal price { get; set; }
        public int category_id { get; set; }
    }
}