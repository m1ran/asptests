﻿using MySQL.Abstract;
using MySQL.Concrete;
using MySQL.Entities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MySQL.Models
{
    public class CategoryModel
    {
        public static SelectList GetCategoriesList()
        {
            MySqlCategoryRepository repository = new MySqlCategoryRepository();
            IEnumerable<Category> items = repository.Categories.OrderBy(m => m.name).ToList();
            return new SelectList(items, "id", "name");
        }
    }
}